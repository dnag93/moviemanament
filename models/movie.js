function Movie(
  maPhim,
  tenPhim,
  biDanh,
  trailer,
  hinhAnh,
  moTa,
  ngayKhoiChieu,
  danhGia
) {
  this.maPhim = maPhim;
  this.tenPhim = tenPhim;
  this.biDanh = biDanh;
  this.trailer = trailer;
  this.hinhAnh = hinhAnh;
  this.moTa = moTa;
  this.maNhom = "GP01";
  this.ngayKhoiChieu = ngayKhoiChieu;
  this.danhGia = danhGia;
}

// mã nhóm ở đây sẽ mặc định là GP01 hết,
// là const nên chỉ khai báo phía dưới
