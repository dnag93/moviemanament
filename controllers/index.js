let movieList = [];

// Hiện chưa xét nhóm phim

let rendeMovieList = list => {
  let listContent = "";
  for (let movie of list) {
    listContent += `
      <div class="col-3">
        <div class="card p-2 mb-3">
          <img src="${
            movie.hinhAnh
          }" style="height:350px; border-radius:5px" alt="">
          <h5>${movie.tenPhim}</h5>
          <p>Đánh giá:
              ${createStar(movie.danhGia)}
          </p>
            <p>N${movie.ngayKhoiChieu}</p>
            <a class="btn btn-success"  href="detail.html?id=${
              movie.maPhim
            }">Chi tiết</a>
            <a class="btn btn-danger mt-2" onclick="deleteMovie(${
              movie.maPhim
            })"> 
            Xóa</a>
        </div>
      </div>
    `;
  }
  document.getElementById("movieListContent").innerHTML = listContent;
};

let createStar = rating => {
  let starRating = "";
  if (rating > 5) rating = 5;
  for (let i = 0; i < rating; i++) {
    starRating += '<i class="fa fa-star text-warning mr-1"></i>';
  }
  for (let i = 0; i < 5 - rating; i++) {
    starRating += '<i class="far fa-star text-warning mr-1"></i>';
  }
  return starRating;
};

const getDataFromDB = () => {
  axios({
    method: "GET",
    url:
      "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01"
  })
    .then(res => {
      //code thành công - resolve
      console.log(res);
      movieList = res.data;
      rendeMovieList(movieList);
    })
    .catch(err => console.log(err.response.data));
};

let createNewMovie = () => {
  const name = document.getElementById("txtName").value;
  const id = document.getElementById("txtId").value;
  const alias = document.getElementById("txtAlias").value;
  const trailer = document.getElementById("txtTrailer").value;
  const image = document.getElementById("txtImage").value;
  const description = document.getElementById("txtDescription").value;
  const releaseDate = document.getElementById("txtReleaseDate").value;
  const rating = document.getElementById("txtRating").value;

  const newMovie = new Movie(
    id,
    name,
    alias,
    trailer,
    image,
    description,
    releaseDate,
    rating
  );

  const accessToken = getAccessToken();

  axios({
    method: "POST",
    url: "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/ThemPhim",
    // body
    data: newMovie,
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  })
    .then(res => {
      getDataFromDB();
      console.log(res);
    })
    .catch(err => console.log(err.response.data));
};

let deleteMovie = id => {
  const accessToken = getAccessToken();

  axios({
    method: "DELETE",
    url: `http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/XoaPhim?MaPhim=${id}`,
    headers:{
      Authorization: `Bearer ${accessToken}`
    }
  })
  .then(res => {
    getDataFromDB();
    console.log(res);
  })
  .catch( err => console.log(err.response.data));
};

let getAccessToken = () =>
  JSON.parse(localStorage.getItem("credentials")).accessToken;

getDataFromDB();

// demo ajax trong js
// const a = 5;
// const b = 7;
// console.log(a);

// setTimeout(function() {
//   console.log(a - 1);
// }, 0);

// setTimeout(function() {
//   console.log(a - 2);
// }, 12000);

// console.log(b);

// function sum() {
//   console.log(a + b);
// }

// sum();