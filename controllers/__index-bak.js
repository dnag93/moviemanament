// API doc
// http://movie0706.cybersoft.edu.vn/swagger/index.html?fbclid=IwAR3QBBurS3BWl1F_l0aOjw0NIeCs_ZRkCaSOuQJDqP6aBhT0YqcrkEqQ00w

// // function getMoviesFromDB (){

// // }

// var getMoviesFromDB = function () {
//     axios({
//         url: 'http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01',
//         method: 'GET'
//     })
//         .then(function (res) {
//             console.log(res)
//             //chay khi lay du lieu thanh cong
//             createMovieList(res.data)
//         })
//         .catch(function (err) {
//             console.log(err);
//         })

// }

// var createMovieList = function (movieArr) {
//     var content = '';
//     for (var i = 0; i < movieArr.length; i++) {
//         var movie = movieArr[i];
//         content += `
//             <div class="col-4">
//                 <div class="card p-3 mb-3">
//                     <img src="${movie.hinhAnh}" style="height:300px" />
//                     <p class="font-weight-bold">${movie.tenPhim}</p>
//                     <p class="lead" style="display: ${ movie.danhGia > 0 ? 'block' : 'none'}">
//                         ${createStars(movie.danhGia)}
//                     </p>
//                     <div class="text-center">
//                         <button class="btn btn-info">Edit</button>
//                         <button class="btn btn-danger" onclick="deleteMovieById(${movie.maPhim})">Delete</button>
//                     </div>
//                 </div>
//             </div>
//         `
//     }
//     document.getElementById('movieListContent').innerHTML = content

// }

// var createStars = function (rating) {
//     var content = '';
//     for (var i = 0; i < rating; i++) {
//         content += `<i class="fa fa-star"></i> `
//     }
//     return content;
// }

// var deleteMovieById = function (movieId) {
//     axios({
//         url: `http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/XoaPhim?MaPhim=${movieId}`,
//         method: 'DELETE',
//         headers: {
//             Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoia2hhaWZlMzIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJRdWFuVHJpIiwibmJmIjoxNTc4MDYwMTk4LCJleHAiOjE1NzgwNjM3OTh9.iz0W79gIqtnQdeRx8yIjQmhA3Ls7rMDOuUXKB99RC_g'
//         }
//     })
//         .then(function (res) {
//             console.log(res);
//         })
//         .catch(function (err) {
//             console.log(err);
//         })
// }

// // console.log('a');
// // setTimeout( function(){
// //     console.log('delay')
// // }, 0 )
// // console.log('b')
// getMoviesFromDB();

// =======================================================================================
