let login = () => {
  const username = document.getElementById("txtUserName").value;
  const password = document.getElementById("txtPassword").value;

  axios({
    method: "POST",
    url: "http://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
    data: {
      taiKhoan: username,
      matKhau: password
    }
  })
    .then(res => {
      console.log(res);
      localStorage.setItem("credentials", JSON.stringify(res.data));
      window.location.assign("index.html");
    })
    .catch(err => console.log(err));
};
