let checkAuthendication = () => {
  const credentials = localStorage.getItem("credentials");
  const isInLoginPage = window.location.pathname === "/views/login.html";

  //   nếu chưa đăng nhập, ko phải login -> vô login
  if (!credentials && !isInLoginPage) {
    window.location.assign("login.html");
  }
  //    nếu chỉ xét chưa đăng nhập về log thì bị loop

  //   nếu đăng nhập rồi và trong login => về home
  if (credentials && isInLoginPage) {
    window.location.assign("index.html");
  }
};

checkAuthendication();
