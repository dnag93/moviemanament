let getIdFromURL = () => window.location.search.substr(1).split("=")[1];

let getDetailMovie = () => {
  axios({
    method: "GET",
    url:
      "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=" +
      getIdFromURL()
  })
    .then(res => {
      console.log(res);
      document.getElementById("movieName").innerHTML = res.data.tenPhim;
      document.getElementById("movieDescription").innerHTML = res.data.moTa;
      document.getElementById("movieImg").setAttribute("src", res.data.hinhAnh);
      document
        .getElementById("movieTrailer")
        .setAttribute("src", res.data.trailer);
    })
    .catch(err => console.log(err.response.err));
};

getIdFromURL();
getDetailMovie();
